import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ChatRoutingModule} from './chat-routing.module';
import {ChatWrapperComponent} from './chat-wrapper.component';
import {ChatUserListComponent} from './components/chat-user-list/chat-user-list.component';
import {ChatWindowComponent} from './components/chat-window/chat-window.component';
import {MaterialModule} from '../material/material.module';

@NgModule({
  declarations: [ChatWrapperComponent, ChatUserListComponent, ChatWindowComponent],
  imports: [CommonModule, ChatRoutingModule, MaterialModule],
})
export class ChatModule {}
