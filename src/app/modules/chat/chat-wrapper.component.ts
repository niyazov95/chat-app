import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-chat-wrapper',
  template: `
    <mat-toolbar color="primary">
      <div class="d-flex w-100 align-items-center justify-content-between">
        <h2>Chat</h2>
        <div class="d-flex align-items-center">
          <button routerLink="/profile" class="mx-2" mat-stroked-button>Settings</button>
          <button routerLink="/" class="mx-2" mat-stroked-button>Logout</button>
        </div>
      </div>
    </mat-toolbar>
    <div class="container pt-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card grey lighten-3 chat-room">
              <div class="card-body">
                <!-- Grid row -->
                <div class="row px-lg-2 px-2">
                  <!-- Grid column -->
                  <div class="col-md-6 col-xl-4 px-0">
                    <app-chat-user-list></app-chat-user-list>
                  </div>
                  <!-- Grid column -->

                  <!-- Grid column -->
                  <div class="col-md-6 col-xl-8 pl-md-3 px-lg-auto px-0">
                    <app-chat-window></app-chat-window>
                  </div>
                  <!-- Grid column -->
                </div>
                <!-- Grid row -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class ChatWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
