import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileWrapperComponent} from './profile-wrapper.component';
import {SettingsComponent} from './components/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileWrapperComponent,
    children: [
      {
        path: '',
        component: SettingsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
