import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile-wrapper',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-12 mt-5 d-flex align-items-center justify-content-center">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class ProfileWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
