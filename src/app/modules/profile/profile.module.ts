import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {SettingsComponent} from './components/settings/settings.component';
import {ProfileWrapperComponent} from './profile-wrapper.component';
import {MaterialModule} from '../material/material.module';

@NgModule({
  declarations: [ProfileWrapperComponent, SettingsComponent],
  imports: [CommonModule, ProfileRoutingModule, MaterialModule],
})
export class ProfileModule {}
