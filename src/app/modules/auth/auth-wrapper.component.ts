import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-auth-wrapper',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-12 min-vh-100 d-flex align-items-center justify-content-center">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styles: [],
})
export class AuthWrapperComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
